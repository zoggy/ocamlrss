OCaml-RSS is an OCaml library to read and write RSS files.
More information on https://zoggy.frama.io/ocamlrss .
